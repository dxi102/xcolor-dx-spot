% \iffalse meta-comment
%
% Copyright (C) 2023 by deimi <deimi@vtex.lt>
% -------------------------------------------------------
%
% This file may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in:
%
%    http://www.latex-project.org/lppl.txt
%
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% \fi
%
% \iffalse
%<*driver>
\ProvidesFile{xcolor-dx-spot.dtx}
%</driver>
%<package>\NeedsTeXFormat{LaTeX2e}[2019/06/01]
%<package>\providecommand\DeclareRelease[3]{}
%<package>\providecommand\DeclareCurrentRelease[2]{}
%
%<package>\DeclareRelease{v1}{2022-12-01}{vtexreadpi-2022-12-01.sty}
%<package>\DeclareCurrentRelease{v2}{2023-01-09}
%
%<package>\ProvidesPackage{xcolor-dx-spot}[2023/01/09 v2 (deimi) spotcolors support]
%<*driver>
\documentclass{ltxdoc}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
%\usepackage[a4paper,margin=25mm,left=20mm,nohead]{geometry}
\usepackage{multicol}
\usepackage[tt=false, sf=false, type1=true]{libertine}
\usepackage[varqu,scaled=.78,mono]{zi4}
\usepackage[libertine]{newtxmath}
\usepackage[tableposition=top]{caption}
\usepackage{xcolor}
\definecolor{back}{cmyk}{1,.29,0,.36}
\def\PS{Postscript}
\usepackage{fancyvrb}
\RecustomVerbatimEnvironment
{Verbatim}{Verbatim}
{gobble=2,
  %rame=single,
  formatcom=\color{back}}
%\DefineShortVerb{\|}
\usepackage{hypdoc}
\usepackage{enumitem}
\setlist%
{%
 topsep=0pt,%
 labelsep=6pt,
 noitemsep,%
 leftmargin=*
}
\setlist[description]{font=\normalfont\sffamily\bfseries,leftmargin=2pc,style=nextline}
\EnableCrossrefs
\CodelineIndex
\RecordChanges
\begin{document}
\DocInput{\jobname.dtx}
\end{document}
%</driver>
% \fi
%
% \GetFileInfo{\jobname.dtx}
%
%
% \title{The \textsf{xcolor-dx-spot} package}
% \author{Deimantas Galčius \\ \texttt{deimi@vtex.lt}}
%
% \maketitle
% \begin{abstract}
%  The aim of the package is to extend |xcolor| package to support spot colors in |dvi->ps->pdf|
%  workflow. For |pdf| workflow spot colors are supported by |colorspace| package.
%  Big part of the code is taken from |colorspace| package and adated for |dvips| workflow. 
% \end{abstract}
%
%  \section{Usage}
%
%  \begin{Verbatim}
%  \usepackage{xcolor}
%  \usepackage{xcolor-dx-spot}
%  \definecolorspace{pantone307u}{PANTONE 307 U}{1.0,.29,.0,.36}
%  \definecolor{blue}{pantone307u}{1}
%  \end{Verbatim}
%
%
%  \subsection{Version 1}
%
%  \begin{Verbatim}
%  \RequirePackage{xcolor-dx-spot}[=v1]
%  \definespotcolor{blue}{PANTONE 307 U}{1.0,.29,.0,.36}
%  \end{Verbatim}
%
% \section{Options}
%
% \begin{description}[style=standard,widest=prologue]
% \item[prologue] pass option |prologue| to |xcolor| package.
% \end{description}
%
% \section{Dependencies}
%
% \begin{itemize}
% \item etoolbox.sty
% \item xcolor.sty
% \item xparse.sty
% \end{itemize}
%
% \section{Technical details on color implementation}
%
% Firstly let us take a short look at how \textbf{cmyk} color is defined via |\definecolor|
% macro (|xcolor|).
%
% 
%  \begin{Verbatim}
%  \definecolor{blue}{cmyk}{1.0,.29,.0,.36}
%  -> \@namedef {\color@blue}{\xcolor@ {}{cmyk 1 0.29 0 0.36}{cmyk}{1,0.29,0,0.36}
%  \@@mod -> cmyk
%  \@@clr -> 1,0.29,0,0.36
%  \@@drv -> cmyk 1 0.29 0 0.36
%  \@@nam -> blue
%  \end{Verbatim}
%  %
%  \noindent |\definecolor{blue}| defines global internal command |\\color@blue|. One can check 
%  the command meaning with |\csshow{\string\color@blue})|. The command holds enough information
%  to extract color \textit{model} (|\@@mod|), normalized \textit{numerical values} (|\@@clr|), \textit{driver spec} (|\@@drv|),
%  and color \textit{name} (|\@@nam|).
%  |\@@drv| value is driver dependend:
%  \begin{Verbatim}
%    \@@drv -> cmyk 1 0.29 0 0.36               % Postscript
%    \@@drv -> 1 0.29 0 0.36 k 1 0.29 0 0.36 K  % PDF       
%  \end{Verbatim} 
%  and is defined by calling |\color@cmyk| macro, which is implemented differently for each driver:
%  see |dvips.def| for \PS, |pdftex.def| for PDF.
%  As we can see, the |\\color@blue| command is driver dependend as
%  |\@@drv| part is driver dependend, and in the process of defining |\\color@blue| the |\color@cmyk|
%  macro is called. As one can guess, in addition driver file defines macros
%  |\color@rgb|, |\color@gray|, |\color@hsb| for different color models as well.
%
%  !!TODO Defining color with tint operator.
%  
%  When one uses color specified, e.g., with |\color{blue}|,
%  the |\@@drv| part is extracted from |\\color@blue| definition
%  and passed to |\set@color| macro, which sets |special| in the
%  output. 
%  In |dvips| workflow |\color{blue}| expands into:
%  \begin{Verbatim}
%    \color{blue}
%    -> \special{color push cmyk 1 0.29 0 0.36}
%  \end{Verbatim}
%  and |dvips| translate this code into Postcript code:
%  \begin{Verbatim}
%    1 0.29 0 0.36 TeXcolorcmyk
%  \end{Verbatim}
%  Similarly for the rgb or gray color model, dvips would translate:
%  \begin{Verbatim}
%                                   (TeX  :-> Postscript)
%  \special{color push rgb  <rgb-values>} :-> <rgb-values> TeXcolorrgb
%  \special{color push gray <gray-value>} :-> <gray-value> TeXcolorgray 
%  \end{Verbatim}
%  In Postscript file we end up with |TeXcolorcmyk| (and |TeXcolorrgb|, |TeXcolorgray|) operators
%  which should be defined.
%  Definitions of these operators are straightforward and are defined in |color.pro| header file:
%  \begin{Verbatim}
%    TeXcolorcmyk => setcmykcolor  PS code: /TeXcolorcmyk{setcmykcolor}def
%    TeXcolorrgb  => setrgbcolor   PS code: /TeXcolorrgb{setrgbcolor}def
%    TeXcolorgray => setgray       PS code: /TeXcolorgray{setgray}def
%    TeXcolorhsb  => sethsbcolor   PS code: /TeXcolorhsb{sethsbcolor}def
%  \end{Verbatim}
%  %
%  \noindent Here |setcmykcolor| is standard Postscript Language Level 2 operator that should
%  be available by Postscript interpreter be it Ghostscript or Distiller.
%
%  Now let us approach  spot colors from bottom to top.
%  Recall that the TeX code |\special{color push cmyk 1 0.29 0 0.36}| gives us
%  |1 0.29 0 0.36 TeXcolorcmyk| in Postscript.
%  If we say |\special{color push foo BAR1 BAR2 BAR3}| in TeX, dvips will translate it into
%  |BAR1 BAR2 BAR3 TeXcolorfoo| Postscript code, where |foo| is arbitrary (our) color space.
%  Let use give a color space name ``ps''  for spot colors.
%  (We could use, e.g., ``spot'' name as well).
%  If we write in \TeX:
%  \begin{Verbatim}
%  \special{color push ps 1 0.29 0 0.36 (PANTONE 307 U) 1}
%  \end{Verbatim}
%   in Postscript we get
%  \begin{Verbatim}
%  1 0.29 0 0.36 (PANTONE 307 U) 1 TeXcolorps
%  \end{Verbatim}
%  where |1 0.29 0 0.36| is color values for an alternative cmyk color space,
%  |(PANTONE 307 U)| is a pantone color name, and value |1| is a tint. (Note that
%  in pantone color definition cmyk values and color name does not change, but
%  the tint can have values [0,1]. In this regard pantone color can be seen
%  like \textit{gray} color space, which has only one variable value.)
%  |TeXcolorps| is unknow operator for PostScript, that we should define.
%  
%  In Postscript we can use |/Separation| colorspaces for spot colors, see [1] (Separation color spaces) 
%  \begin{Verbatim}
%  [/Separation name alternativeSpace tintTransform] setcolorspace
%  \end{Verbatim}
%
%  One can find and example of such colorspace on [1] p.245
% 
%  \begin{Verbatim}
% [ /Separation
% (LogoGreen) % Spot color name
% /DeviceCMYK % alternative color space
% {
%     dup
%     0.84 mul % c=.84
%     exch
%     0.0      % m=.0
%     exch
%     dup
%     0.44 mul % y=.44
%     exch
%     0.21 mul % k=.21
% }
% ] setcolorspace
%  \end{Verbatim}
%  We can wrap the code above into operator |/setcustomcolor|:
%  \begin{Verbatim}
%  /setcustomcolor {% customcolorarray tint
%    exch
%    [ exch /Separation exch dup 4 get exch /DeviceCMYK exch
%      0 4 getinterval
%      [ exch /dup load exch cvx {mul exch dup} 
%        /forall load /pop load dup
%      ] cvx
%    ] setcolorspace setcolor
%  } bind def
%  \end{Verbatim}
%  Now we can write Postscript header  for |dvips|:
%    \begin{macrocode}
%<*psheader>
/setcustomcolor {% customcolorarray tint
exch
[exch /Separation exch dup 4 get exch /DeviceCMYK exch
 0 4 getinterval
 [exch /dup load exch cvx {mul exch dup}
   /forall load /pop load dup] cvx
 ] setcolorspace setcolor
} bind def
/findcmykcustomcolor where{pop}{/findcmykcustomcolor {5 array astore readonly}bind def}ifelse
/TeXcolorps{/tint exch def findcmykcustomcolor currentoverprint setoverprint tint setcustomcolor}def
%</psheader>
%    \end{macrocode}
%  In addition the header defines |/findscmykcustomcolor| operator in case it is not defined and
%  sets current |overprint| value for color.
%
%  At this point loading header file (|xcolor-dx-spot.pro|) in TeX file and
%  with |\special| command we can poduce spot colors in PDF file:
%  \begin{Verbatim}
%    \AtBeginDvi{\special{header=xcolor-dx-spot.pro}}
%    \special{color push ps 1 0.29 0 0.36 (PANTONE 307 U) 1}
%    Text
%    \special{color pop}
%  \end{Verbatim}
%  
%  
%
% \section{Package code}
%
%<*package>
%
% Define option 'prologue'
%
%    \begin{macrocode*}
\DeclareOption{prologue}{\PassOptionsToPackage{prologue}{xcolor}}
\ProcessOptions*
%    \end{macrocode*}
%
% Load dependency packages
%
%    \begin{macrocode*}
\RequirePackage{etoolbox}
\RequirePackage{xparse}
\RequirePackage{xcolor}
%    \end{macrocode*}
%
% Load Postscript header file
% 
%    \begin{macrocode*}
\AtBeginDvi{\special{header=xcolor-dx-spot.pro}}
%    \end{macrocode*}
%
%
%    \begin{macrocode}
%
\ExplSyntaxOn
\def\define@color@named#1#2{\let\@@cls\@empty\@namedef{\string\color@#1}{{}{}{#2}{}{}}}
%
% \definecolorspace{pantone307u}{PANTONE 307 U}{1.0,.29,.0,.36}
%
% \makeatletter
% \def\color@spotcolor#1#2{\c@lor@@spotcolor#2\@@#1}
% \def\c@lor@@spotcolor#1,#2\@@#3{%
%   \c@lor@arg{#2}%
%   \edef#3{/#1 cs /#1 CS #2 sc #2 SC}%
% }
% \makeatother

% Extension to dvips.def
%
\def\color@spot@cmyk#1#2{\c@lor@@spotcmyk #2\@@ #1}
\def\c@lor@@spotcmyk#1,#2,#3,#4\@@ #5{
  \c@lor@arg{#4}
  \c@lor@arg{#1}
  \c@lor@arg{#2}
  \c@lor@arg{#3}
  \edef#5{ps ~#1 ~#2 ~#3 ~#4}
}
% End extension to dvips
%
\def\spc@cmyktospot#1#2{%
  \begingroup
    \def\spc@tempc ~cmyk ~##1 ~##2 ~##3 ~##4\@@{##1 ~##2 ~##3 ~##4}%
    \def\xcolor@##1##2##3##4{%
      \xdef\spc@tempa{\spc@tempc##2\@@}% ->1 0.29 0 0.36.
%      \expandafter\protected@xdef\csname\string\color@#1\endcsname{%
%        \noexpand\xcolor@{##1}{ps \space \spc@tempa \space #2 \space 1}{spot}{1}}%
      \expandafter\xdef\csname spc@ascmyk@#1\endcsname{##4}}%
    \csname\string\color@#1\endcsname
%    \csshow{\string\color@blue}% \xcolor@ {}{ps 1 0.29 0 0.36(PANTONE 307 U) 1}{spot}{1}.
%    \csshow{spc@ascmyk@#1}% ->1,0.29,0,0.36.
%    \show\spc@tempa
  \endgroup}
\def\DXC@get@cmyk#1#2#3#4{%
  % - (1) define color as cmyk with \definecolor
  % - (2) extract normalised cmyk values
  \cslet{spc@infl@#1}\relax
  \definecolor{dspot@temp}{cmyk:cmyk}{#3}% 
  \expandafter\spc@cmyktospot{dspot@temp}{(#2)} % Convert cmyk > spot
  \global\let#4\spc@tempa\relax
}
%
%
\def\spc@getir#1{%
  \begingroup
    \def\xcolor@##1##2##3##4{\spc@getref##2\@@}%
    \@nameuse{#1}%
  \endgroup}
\def\spc@getref ps #1 (#2) #3\@@{\gdef\spc@ir{#1 (#2)}}
%
\def\spc@infl@cmyk{\let\spc@inflcommas\@empty}% 4 values=4-4 commas
\def\spc@infl@gray{\def\spc@inflcommas{,,,}}% 1 value=4-1 commas
\def\spc@infl@spot{%
  \spc@getir{\string\color@\@@nam}%
  \def\spc@inflcommas{,,,}}%
%
\def\XC@inflate#1#2#3#4{%
  \def\spc@inflcommas{,}%      Assume 3 values=4-3 commas
  \@nameuse{spc@infl@\@@mod}%
  \edef#4{#2,\spc@inflcommas#3\spc@inflcommas}}
%
%\XC@sdef\XC@mod@spot{spot}
%\def\XC@clr@spot@white{0}
% Extension to xspace.sty
%
%\definecolorspace{pantone307u}{PANTONE 307 U}{1.0,.29,.0,.36}
\NewDocumentCommand\definecolorspace{ m m m }{
  \xdef\@@spotname{#2}% PANTONE 307 U
  % \color@spot@cmyk\@@drv{#3}% \@@drv -> ps 1.0 0.29 0.0 0.36
  \begingroup
  \DXC@get@cmyk{#1}{#2}{#3}\@@drv
  \endgroup
  \csxdef{XC@mod@#1}{ps}
  \csxdef{XC@drv@#1}{ps\space\@@drv\space(\@@spotname)}%{ps 1.0 0.29 0.0 0.36 (PANTONE 307 U)}%
  \csgdef{XC@clr@#1@white}{0}%
  % pgfsys addition start
  \global\cslet{pgfsys@color@#1}\pgfsys@color@spot
  \global\cslet{pgfsys@color@#1@stroke}\pgfsys@color@spot@stroke
  \global\cslet{pgfsys@color@#1@fill}\pgfsys@color@spot@fill
  % pgfsys addition stop
  \global\cslet{spc@infl@#1}\spc@infl@spot % for blue!50
  \csxdef{spc@ir@#1}{\@@drv\space(\@@spotname)}%{1.0 0.29 0.0 0.36 (PANTONE 307 U)}%
%  \global\cslet{color@#1}\color@spot % for pgf
%  \csxdef{spc@infl@#1}{\spc@infl@spot}
%  \csshow{spc@infl@#1}
  % \csgdef{spc@infl@#1}{
  %   \FOO
  %   \show\@@nam
  %   \spc@getir{\string\color@\@@nam}
  %   \def\spc@inflcommas{,,,}
  % }% 
  %
  % similarly to \color@cmyk
  \edef\@@tmp{%
    \noexpand\gdef\csname color@#1\endcsname####1####2
    {
      \noexpand\xdef\noexpand\spc@ir{\csuse{spc@ir@#1}}
      \noexpand\c@lor@arg{####2}\noexpand\edef####1{\csuse{XC@drv@#1} ~####2}}}
%  \show\@@tmp
%  \show\color@cmyk
  \@@tmp
 %
}
%
\NewDocumentCommand\spotcolorlet{ m m }
{
  \colorlet[ps]{#1}[ps]{#2}
}
%
% \definecolor{blue}{pantone307}{1}
% -> \@namedef {\color@blue}{\xcolor@ {}{ps 1.0 .29 .0 .36 (PANTONE 307 U) 1}{pantone307u}{1}}
%
% \@@mod -> pantone307u
% \@@clr -> 1
% \@@drv -> ps 1.0 .29 .0 .36 (PANTONE 307 U) 1
% \@@nam -> blue
%
%
% pgfsys begin
\def\pgfsys@color@spot@stroke#1{%
  \pgfsysprotocol@literal{/pgfsc{\spc@ir ~#1~TeXcolorps}def}\pgf@strokecolortrue}%
\def\pgfsys@color@spot@fill#1{%
  \pgfsysprotocol@literal{/pgffc{\spc@ir ~#1~TeXcolorps}def}\pgf@fillcolortrue}%
\def\pgfsys@color@spot#1{%
  \pgfsys@color@reset
  \pgfsysprotocol@literal{\space\spc@ir \space#1\space TeXcolorps}%
  \pgfsys@color@spot@stroke{#1}%
  \pgfsys@color@spot@fill{#1}}

% pgfsys end
% dspo3
\XC@sdef\XC@mod@spot{spot}
\expandafter\XC@sdef\csname XC@mod@pantone307u\endcsname{spot}
\def\XC@clr@spot@white{0}
\csdef{XC@clr@pantone307u@white}{0}
\def\color@spot#1#2{%
%  \OOO
%  MAYBE use \spc@ir@BLUE
  \show\spc@ir
  \show\current@color
  \ifx\spc@ir\@undefined
    \expandafter\spc@getref\current@color\@@
  \fi
  \c@lor@arg{#2}%
  \edef#1{ps \spc@ir\space #2}}
%\cslet{color@pantone307u}\color@spot
\ExplSyntaxOff
%    \end{macrocode}
%</package>
% \Finale
\endinput
%%Local Variables:
%% mode: docTex
%% End:
